module.exports = {
 title: 'GitLab ❤️ VuePress',
 description: 'Vue-powered static site generator running on GitLab Pages',
 base: '/vuepress/',
 dest: 'public'
}
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTYxMzIxMDg5NF19
-->